# gitlab-runner-qrb5165-host-tools

## **Getting started**
[What is a gitlab runner?](https://docs.gitlab.com/runner/)

This repository contains the scripts for installing all dependencies that are necessary for a host computer to run CI jobs as well as registering, creating the docker image for, and removing a gitlab runner. All you need to do is run the command `./setup.sh` and the script will take care of the rest.

----
## **Script/File Overviews:**
## Setup
This script will install all necessary dependencies and provide the option to create a gitlab runner which can run CI/CD jobs. 

It can be used as shown below:
```bash
./setup.sh 
```
The following dependencies are installed :
- ADB
- Fastboot
- Docker
- Git
- Gitlab-runner 
- Ifconfig
- Jq
- pip3
- Python Requests lib
- Python Serial lib
- Python PySerial lib 
- VS Code
- Tmux 

#### **Note:** This list above may grow as other dependencies can be included over time as needed. 


**The gitlab runner registration process will ask for the following information**:
- **Description**: Generally the name which you want to assign to the runner being registered. Ex: TF-D0008
- **Tags**: Used to configure what kind of jobs this runner can execute. These tags can be added to jobs in your project's .gitlab-ci.yml and when a CI/CD job runs, it knows which runner to use for each job by checking these tags. Ex: D0008
- **Gitlab Instance URL**: A link to your Gitlab instance. Found in Settings > CI/CD > Runners > Project runners. 
- **Registration Token**: A 29 character token used to register a gitlab runner. Found in Settings > CI/CD > Runners > Project runners.

----

## Register-runner
This script is used to register a gitlab runner, it is called by the main setup script and shouldn't need to be called directly unless setting up more than one gitlab runner on the host computer.

It can be used as shown below:
```bash
./register-runner.sh [Description] [Tag(s)] [Gitlab Instance URL] [Registration Token] 
```
**Arguments**:
- **Description**: Generally the name which you want to assign to the runner being registered. Ex: TF-D0008
- **Tags**: Used to configure what kind of jobs this runner can execute. These tags can be added to jobs in your project's .gitlab-ci.yml and when a CI/CD job runs, it knows which runner to use for each job by checking these tags. Ex: D0008
- **Gitlab Instance URL**: A link to your Gitlab instance. Found in Settings > CI/CD > Runners > New project runner. 
- **Registration Token**: A 29 character token used to register a gitlab runner. Found in Settings > CI/CD > Runners > New project runner.

----

## Unregister-runner
This script is used to remove a gitlab runner, **removing a runner in an irreversible action so be careful**. This can also be done from the gitlab website.

It can be used as shown below:
```bash
./unregister-runner.sh [Gitlab Instance URL] [Runner Token] 
```
**Arguments**:
- **Gitlab Instance URL**: A link to your Gitlab instance. Found in Settings > CI/CD > Runners > Project runners. 
- **Runner Token**: A 28 character token used to identify a gitlab runner. 

#### **Note**: 
- This gitlab runner token can be found by running `sudo cat /etc/gitlab-runner/config.toml`. If you ran the install script without sudo then the token can be found by running `cat ~/.gitlab-runner/config.toml`.

----


## Depricated

The old `setup.sh` script that sets up voxl-provisioning-node has been moved to `old-setup.sh`

### .env
#### **Note:** This file is currently not used in the CI host computer set up process and the system image information in it may be out of date.
This file is only used for generating docker images and contains:
- Name and tag for the docker image that will be loaded on the gitlab runner
- Name and gsutil link for VOXL2 platform release
- Name and gsutil link for RB5 platform release

These platform releases may need to be downloaded from the modalai website and placed in a "system-images" folder in order to be mounted to the docker image:
- VOXL2: https://developer.modalai.com/asset/2
- RB5: https://developer.modalai.com/asset/3

----

### docker-build-image
This script will build a docker image which is then loaded into the gitlab runner being created.
```bash
./docker-build-image.sh
```
#### **Note:** This file is currently not used in the CI host computer set up process and the system image information it pulls from .env may be out of date. When you want to run a job using docker as the executor you must run `adb kill server` on the device which hosts the gitlab runner so that the docker container the runner uses can communicate with the target.

If during the installation process NVM fails to be recognized, run the command below to load NVM into the host computers env and then re-run the setup script.
```bash
source $HOME/.nvm/nvm.sh
```

old dependencies list:

- Node.js
- Npm (Node Package Manager)
- Nvm (Node Version Manager)
- Node.js Request pkg