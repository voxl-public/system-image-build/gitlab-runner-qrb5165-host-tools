#!/bin/bash

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# Colors, bold, underline, etc
CLR_RED="\033[1;31m"
CLR_YLW="\033[1;33m"
CLR_GRY="\033[1;90m"
CLR_GRN="\033[1;32m"
CLR_LIT_GRN="\033[1;92m"
SUCCESS="\xE2\x9C\x94"
FAILURE="\xE2\x9D\x8C"
SET_UNDERLINE="\033[4m"
RESET_UNDERLINE="\033[24m"
RESET_ALL="\033[0m"

# Args
GITLAB_INSTANCE_URL="$1"
RUNNERTOKEN="$2"

# ------------------------------------------------------- 
# print usage                            
# ------------------------------------------------------- 
function _print_usage () {
    echo "======================================================================================================="
    echo ""
	echo " Usage:"
    echo ""
    echo "   Uninstall or remove a runner from gitlab"
	echo "   You must run ./install.sh first to register a valid runner to remove or have the relevant"
	echo "   information for a gitlab runner that has previously been registered."
	echo ""
    echo "   ./uninstall.sh [Runner Token] [Gitlab Instance URL]"
    echo ""
    echo " Optional Arguments:"
    echo ""
    echo "   -h, --help              Show this help message and exit"
    echo ""
	echo " Required Arguments:"
    echo ""
	echo "   Runner Token -          28 character token used to identify a gitlab runner. This token"
	echo "                           can be found by running sudo cat /etc/gitlab-runner/config.toml"
    echo ""
    echo "   Gitlab Instance URL -   Found in Settings > CI/CD > Runners > Project runners."
    echo "                           A link to your Gitlab instance. "
    echo ""
    echo "======================================================================================================="
}

# ------------------------------------------------------- 
# argument parser                               
# ------------------------------------------------------- 
function _parse_opts() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                _print_usage
                exit 0
                ;;
            *)
                ;;
        esac
        shift
    done
}

_parse_opts $@

# Check that arguments necessary for registration of a runner are passed in
if [[ $# -lt 2 ]]; then
    echo -e "${CLR_RED}[ERROR]   Missing input arguments, must include all 2: ./uninstall.sh [RUNNER TOKEN] [GITLAB_INSTANCE_URL]${RESET_ALL}"
    _print_usage
    exit 1
fi

# Check gitlab-runner installed 
if [[ $(which gitlab-runner) ]]; then
    echo -e "${CLR_GRN}[INFO] Gitlab Runner is installed.${RESET_ALL}"
else
    echo -e "${CLR_RED}[ERROR] Gitlab Runner not installed. Please install Gitlab Runner: https://docs.gitlab.com/runner/install/${RESET_ALL}"
    exit 1
fi

echo ""
echo -e "\t${CLR_RED}*** [WARNING] Unregistering a runner is an irreversible action ***${RESET_ALL}"
echo ""

echo -e "${CLR_YLW}Would you like to continue to unregister token: ${CLR_GRN}${1} ${CLR_YLW}?${RESET_ALL}"
select yn in "Yes" "No"; do
    case $yn in
        Yes )
            break
            ;;
        No )
            exit -1
            ;;
    esac
done

sudo gitlab-runner unregister --url ${GITLAB_INSTANCE_URL} --token ${RUNNERTOKEN}

if [[ "$?" != "0" ]]; then
    echo -e "${CLR_RED}[ERROR] Failed to unregister runner with token: ${1}${RESET_ALL}"
    exit 1
else
    echo -e "${CLR_GRN}[INFO] Succesfully unregistered runner with token: ${1}${RESET_ALL}"
fi