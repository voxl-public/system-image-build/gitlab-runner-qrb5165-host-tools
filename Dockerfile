FROM ubuntu:18.04

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

# Bunker Application
RUN pip3 install packaging
RUN pip3 install firebase_admin
RUN pip3 install pyserial

# VOXL connection and flashing
RUN apt-get install -y android-tools-adb android-tools-fastboot

WORKDIR /

RUN mkdir -p /runner
RUN mkdir -p /runner/system-images

WORKDIR /runner

CMD [ "/bin/bash" ]
