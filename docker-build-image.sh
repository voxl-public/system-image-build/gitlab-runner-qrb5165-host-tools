#!/bin/bash

source .env

if [[ $(which docker) ]]; then
    echo "[INFO]    Docker installed"
else
    echo "[ERROR]   Docker is required, please install: https://docs.docker.com/get-docker/"
    exit 1
fi

echo "[INFO]    Building image: ${IMAGE_NAME}:${IMAGE_TAG}"

# Build image
docker build -t "${IMAGE_NAME}:${IMAGE_TAG}" .
