#!/bin/bash

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

source .env

# Colors, bold, underline, etc
CLR_RED="\033[1;31m"
CLR_YLW="\033[1;33m"
CLR_GRY="\033[1;90m"
CLR_GRN="\033[1;32m"
CLR_LIT_GRN="\033[1;92m"
SUCCESS="\xE2\x9C\x94"
FAILURE="\xE2\x9D\x8C"
SET_UNDERLINE="\033[4m"
RESET_UNDERLINE="\033[24m"
RESET_ALL="\033[0m"

# Args
DESCRIPTION="$1"
TAGS="$2"
GITLAB_INSTANCE_URL="$3"
AUTHTOKEN="$4"

# ------------------------------------------------------- 
# print usage                            
# ------------------------------------------------------- 
function _print_usage () {
    echo -e "${CLR_YLW}======================================================================================================="
    echo ""
    echo " Usage:"
    echo ""
    echo "   Create a gitlab runner which can run jobs given a description, tags, gitlab instance"
    echo "   URL, and an authorization token. Runners are configured with the shell executor"
    echo "   through this script are only configured to run with the bash shell."
    echo ""
    echo "   ./install.sh [Description] [Tags] [Gitlab Instance URL] [Registration Token]"
    echo ""
    echo " Optional Arguments:"
    echo ""
    echo "   -h, --help              Show this help message and exit."
    echo ""
    echo " Required Arguments:"
    echo ""
    echo "   Description -           A description or name for the runner being registered."
    echo ""
    echo "   Tags -                  Tags can be used to configure what kind of jobs this runner can"
    echo "                           run. These tags can be added to jobs in your .gitlab-ci.yml and"
    echo "                           when a CI/CD job runs, it knows which runner to use by checking"
    echo "                           these tags."
    echo ""
    echo "   Gitlab Instance URL -   Found in Settings > CI/CD > Runners > Project runners."
    echo "                           A link to your Gitlab instance. "
    echo ""
    echo "   Registration Token -    Found in Settings > CI/CD > Runners > Project runners."
    echo "                           A 29 character token used to register a gitlab runner. "
    echo ""
    echo -e "=======================================================================================================${RESET_ALL}"
}

# ------------------------------------------------------- 
# argument parser                               
# ------------------------------------------------------- 
function _parse_opts() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                _print_usage
                exit 0
                ;;
            *)
                ;;
        esac
        shift
    done
}

_parse_opts $@

# Check that arguments necessary for registration of a runner are passed in
if [[ $# -lt 4 ]]; then
    echo -e "${CLR_RED}[ERROR]   Missing input arguments, must include all 4: ./install.sh [DESCRIPTION] ['TAGS'] [GITLAB_INSTANCE_URL] [AUTHTOKEN]"
    echo "The following were provided:"
    echo -e "\tDESCRIPTION: $1"
    echo -e "\tTAGS: $2"
    echo -e "\tGITLAB_INSTANCE_URL: $3"
    echo -e "\tAUTHTOKEN: $4${RESET_ALL}"
    _print_usage
    exit 1
fi

# Register a gitlab runner 
sudo gitlab-runner register \
    --non-interactive \
    --url "${GITLAB_INSTANCE_URL}" \
    --registration-token "${AUTHTOKEN}" \
    --executor "shell" \
    --shell "bash" \
    --description "${DESCRIPTION}" \
    --maintenance-note "CI RACK GITLAB RUNNER" \
    --tag-list "${TAGS}" \
    --run-untagged="false" \
    --locked="false" \
    --access-level="not_protected" \

echo "Setting gitlab-runner user perms"
sudo usermod -aG sudo gitlab-runner
sudo usermod -aG plugdev gitlab-runner
sudo usermod -aG dialout gitlab-runner