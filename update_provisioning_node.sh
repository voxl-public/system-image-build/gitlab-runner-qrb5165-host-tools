#!/bin/bash

echo "Updating and restarting voxl-provisioning-node"

# Kill current instance of provisioning node
tmux list-sessions
if [ $? -eq 0 ]; then
    tmux kill-session -t $(tmux list-sessions | cut -d ':' -f 1)
else
    kill $(ps -a | grep node | awk '{print $1}')
fi

# Update provisioning node
cd ~/repos/voxl-provisioning-node
git pull

# Start provisioning node up again
tmux new-session -d -s node './run-local.sh'

echo "Done updating and restarting voxl-provisioning-node"
