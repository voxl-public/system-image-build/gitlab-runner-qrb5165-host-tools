#!/bin/bash

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# Colors, bold, underline, etc
CLR_RED="\033[1;31m"
CLR_YLW="\033[1;33m"
CLR_GRY="\033[1;90m"
CLR_GRN="\033[1;32m"
CLR_LIT_GRN="\033[1;92m"
CLR_RESET="\033[39m"
SUCCESS="\xE2\x9C\x94"
FAILURE="\xE2\x9D\x8C"
SET_BOLD="\033[1m"
SET_UNDERLINE="\033[4m"
RESET_UNDERLINE="\033[24m"
RESET_ALL="\033[0m"

# ------------------------------------------------------- 
# print usage                            
# ------------------------------------------------------- 
function _print_usage () {
    echo -e "${CLR_YLW}======================================================================================================="
    echo ""
    echo " Description:"
    echo ""
    echo "   This script sets up a computer to be able to run as a host on the CI rack. Necessary"
    echo "   tools and packages such as ADB, fastboot, and Node.js will be installed on the host."
    echo "   At the end of the dependency installation process, the option to create a gitlab"
    echo "   runner which can run CI jobs given a description, tags, gitlab instance URL, and an"
    echo "   authorization token. Runners are configured with the shell executor and are only"
    echo "   configured to run with the bash shell."
    echo ""
    echo "Usage: "
    echo "   ./setup.sh"
    echo ""
    echo " Optional Arguments:"
    echo ""
    echo "   -h, --help              Show this help message and exit."
    echo ""
    echo -e "=======================================================================================================${RESET_ALL}"
}

# ------------------------------------------------------- 
# argument parser                               
# ------------------------------------------------------- 
_parse_opts() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                _print_usage
                exit 0
                ;;
            *)
                ;;
        esac
        shift
    done
}

# ------------------------------------------------------- 
# register gitlab runner given user inputs
# ------------------------------------------------------- 
function register_gitlab_runner() {
    echo ""
    echo "======================================================================================================="
    echo -e "${RESET_ALL}"
    echo -e "${CLR_GRN}[INFO]    Begin gitlab ci runner setup."
    echo ""
    echo -e "${CLR_LIT_GRN}Please enter a description for the runner (Ex:TF-D0008):${RESET_ALL}"
    read DESCRIPTION
    echo ""
    echo -e "${CLR_LIT_GRN}Please enter a tag for this runner (what type of jobs it will be able to run):${RESET_ALL}"
    read TAGS
    echo ""
    echo -e "${CLR_LIT_GRN}Please enter the gitlab instance URL:${RESET_ALL}"
    read GITLAB_INSTANCE_URL
    echo ""
    echo -e "${CLR_LIT_GRN}Please enter the gitlab authorization token:${RESET_ALL}"
    read AUTHTOKEN
    echo ""
    echo "======================================================================================================="
    echo -e "${RESET_ALL}"

    echo -e "${SET_BOLD}Provided gitlab runner registration args:${SET_BOLD}"
    echo -e "\t${CLR_RESET}DESCRIPTION: ${CLR_LIT_GRN}$DESCRIPTION"
    echo -e "\t${CLR_RESET}TAGS: ${CLR_LIT_GRN}$TAGS"
    echo -e "\t${CLR_RESET}GITLAB_INSTANCE_URL: ${CLR_LIT_GRN}$GITLAB_INSTANCE_URL"
    echo -e "\t${CLR_RESET}AUTHTOKEN: ${CLR_LIT_GRN}$AUTHTOKEN"
    echo -e "${RESET_ALL}"

    echo -e "${CLR_LIT_GRN}Would you like to continue with the args provided above?${RESET_ALL}"
    select opt in "Yes" "No" "Exit"; do
        case $opt in
            "Yes" )
                echo ""
                echo -e "${CLR_GRN}[INFO]   Beginning gitlab runner registration process.${RESET_ALL}"
                echo ""
                ./register-runner.sh $DESCRIPTION $TAGS $GITLAB_INSTANCE_URL $AUTHTOKEN
                if [ "$?" -ne 0 ]; then
                    echo -e "${CLR_RED}[ERROR]   Failed to install register gitlab runner\t${FAILURE}${RESET_ALL}"
                    exit 14
                else
                    echo -e "${CLR_GRN}[INFO] CI setup successful!\t${SUCCESS}${RESET_ALL}"
                fi
                break;;

            "No" )
                register_gitlab_runner
                break;;

            "Exit" )
                echo -e "${CLR_GRN}[INFO] Exiting ci runner set up.${RESET_ALL}"
                echo ""
                exit 0;;

            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;

        esac
    done
}

# ------------------------------------------------------- 
# set up gitlab ssh key
# ------------------------------------------------------- 
function setup_gitlab_ssh_key() {
    echo "" 
    echo -e "${CLR_LIT_GRN}Beginning Gitlab SSH key process...${RESET_ALL}"
    ssh-keygen -t ed25519 -f /home/$USER/.ssh/id_ed25519
    echo ""
    echo ""
    echo -e "${CLR_LIT_GRN}SSH Key:${RESET_ALL}"
    cat ~/.ssh/id_ed25519.pub
    echo ""
    echo -e "${CLR_LIT_GRN}Please enter the SSH key shown above into the "Key" field in https://gitlab.modalai.com/-/profile/keys and then press any key to continue.${RESET_ALL}"
    read
    echo ""
    
    echo -e "${CLR_LIT_GRN}Testing SSH key connection...${RESET_ALL}"
    ssh -T git@gitlab.modalai.com
    if [ $? -eq 0 ]; then
        echo -e "${CLR_GRN}[INFO] Gitlab SSH key setup successful!\t${SUCCESS}${RESET_ALL}"
    else 
        echo -e "${CLR_RED}[ERROR]   Failed to set Gitlab SSH key\t${FAILURE}${RESET_ALL}"
    fi
}

_parse_opts $@

# Update apt and prepare workspace...
echo -e "${CLR_GRN}[INFO]    Updating apt and preparing workspace (this may take a few seconds)...${RESET_ALL}"
sudo add-apt-repository -y universe &> /dev/null
sudo apt-get remove brltty -y &> /dev/null
sudo apt-get remove docker docker-engine docker.io -y &> /dev/null
sudo snap install --classic code &> /dev/null
sudo sed -i '/cdrom/d' /etc/apt/sources.list &> /dev/null
sudo usermod -aG sudo $USER
sudo usermod -aG plugdev $USER
sudo usermod -aG dialout $USER
sudo apt-get -q update  &>/dev/null
if ! [ -d ~/repos ]; then
    mkdir ~/repos
fi
echo -e "${CLR_GRN}[INFO]    Workspace preparation complete!\t${SUCCESS}${RESET_ALL}"

# Check that we have all necessary dependencies
echo -e "${CLR_GRN}[INFO]    Checking dependencies...${RESET_ALL}"

# ADB
if [[ $(which adb) ]]; then
    echo -e "${CLR_GRN}[INFO]    ADB installed\t\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    ADB not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install -y android-tools-adb ...${RESET_ALL}"
    sudo apt-get install -y android-tools-adb
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install ADB\t${FAILURE}${RESET_ALL}"
        exit 1
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed ADB${RESET_ALL}"
    fi
fi

# Fastboot
if [[ $(which fastboot) ]]; then
    echo -e "${CLR_GRN}[INFO]    Fastboot installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Fastboot not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install -y android-tools-fastboot ...${RESET_ALL}"
    sudo apt-get install -y android-tools-fastboot
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Fastboot\t${FAILURE}${RESET_ALL}"
        exit 2
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Fastboot${RESET_ALL}"
    fi
fi

# Docker
if [[ $(which docker) ]]; then
    echo -e "${CLR_GRN}[INFO]    Docker installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Docker not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install docker.io -y ...${RESET_ALL}"
    sudo apt-get install docker.io -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install docker.io\t${FAILURE}${RESET_ALL}"
        exit 3
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed docker.io${RESET_ALL}"
    fi
    echo -e "${CLR_YLW}[WARN]    Running: sudo snap install docker -y ...${RESET_ALL}"
    sudo snap install docker 
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install docker\t${FAILURE}${RESET_ALL}"
        exit 4
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed docker\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Git
if [[ $(which git) ]]; then
    echo -e "${CLR_GRN}[INFO]    Git installed\t\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Git not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install git -y ...${RESET_ALL}"
    sudo apt-get install git -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install git\t${FAILURE}${RESET_ALL}"
        exit 5
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed git\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Gitlab-runner and dependencies
if [[ $(which gitlab-runner) ]]; then
    echo -e "${CLR_GRN}[INFO]    Gitlab Runner installed\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Gitlab Runner is required"
    echo -e "${CLR_YLW}[WARN]    Installing Gitlab Runner now${RESET_ALL}"

    # Curl comes with linux distros so should always be present... but just in case
    if [[ $(which curl) ]]; then
        echo -e "${CLR_GRN}[INFO]    Curl installed${RESET_ALL}"
    else
        echo -e "${CLR_YLW}[WARN]    Curl is required for Gitlab Runner intallation"
        echo -e "${CLR_YLW}[WARN]    Installing Curl now"
        echo -e "${CLR_YLW}[WARN]    Running: sudo apt install curl ...${RESET_ALL}"
        sudo apt install curl
        if [ "$?" -ne 0 ]; then
            echo -e "${CLR_RED}[ERROR]   Failed to install Curl\t${FAILURE}${RESET_ALL}"
            exit 6
        else 
            echo -e "${CLR_GRN}[INFO]    Successfully installed Curl\t${SUCCESS}${RESET_ALL}"
        fi
    fi
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install gitlab-runner -y ...${RESET_ALL}"
    sudo apt-get install gitlab-runner -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Gitlab Runner\t${FAILURE}${RESET_ALL}"
        exit 7
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Gitlab Runner\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Node.js
if [[ $(which node) ]]; then
    echo -e "${CLR_GRN}[INFO]    Node.js installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Node.js not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install nodejs -y ...${RESET_ALL}"
    sudo apt-get install nodejs -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Node.js\t${FAILURE}${RESET_ALL}"
        exit 5
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Node.js\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Npm (Node.js package manager)
if [[ $(which npm) ]]; then
    echo -e "${CLR_GRN}[INFO]    Npm installed\t\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Npm not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install npm -y ...${RESET_ALL}"
    sudo apt-get install npm -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Npm\t${FAILURE}${RESET_ALL}"
        exit 5
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Npm\t${SUCCESS}${RESET_ALL}"
    fi
fi

source $HOME/.nvm/nvm.sh

# NVM (Node.js version manager)
if [[ $(nvm --version) ]]; then
    echo -e "${CLR_GRN}[INFO]    Nvm installed\t\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Nvm not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash ...${RESET_ALL}"
    curl -s -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    if [[ $SHELL == *"bash"* ]]; then
        echo -e "${CLR_GRN}[INFO]    Bash detected${RESET_ALL}"
        source ~/.bashrc
    elif [[ $SHELL == *"zsh"* ]]; then
        echo -e "${CLR_GRN}[INFO]    Zsh detected${RESET_ALL}"
        export NVM_DIR="$HOME/.nvm"
        [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
        [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion
        # zsh
    else
        echo -e "${CLR_YLW}[WARN]   Unknown shell detected. Might need to source your shell rc manually.${RESET_ALL}"
    fi

    nvm --version
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Nvm\t${FAILURE}${RESET_ALL}"
        exit 5
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Nvm\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Set correct version of Node.js
nvm install v17.9.1
nvm alias default v17.9.1
nvm use
NODE_VER=$(node -v)
if [[ $NODE_VER != "v17.9.1" ]]; then
    echo -e "${CLR_RED}[ERROR]   Failed to set correct Node.js version: $NODE_VER.\t${FAILURE}${RESET_ALL}"
else
    echo -e "${CLR_GRN}[INFO]    Set Node.js version: $NODE_VER\t${SUCCESS}${RESET_ALL}"
fi

# Node.js Request package
if [ "$(npm list | grep request)" ]; then
    echo -e "${CLR_GRN}[INFO]    Node.js Request package\t${SUCCESS}${RESET_ALL}"
else    
    echo -e "${CLR_YLW}[WARN]    Node.js Request package not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: npm i request ...${RESET_ALL}"
    npm i request
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to set install Node.js Request package using npm.${FAILURE}${RESET_ALL}"
        exit 6
    else
        echo -e "${CLR_GRN}[INFO]    Node.js Request package\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Net-tools - ifconfig
if [[ $(which ifconfig) ]]; then
    echo -e "${CLR_GRN}[INFO]    Net-tools installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Net tools not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install net-tools -y ...${RESET_ALL}"
    sudo apt-get install net-tools -y
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install net-tools\t${FAILURE}${RESET_ALL}"
        exit 8
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed net-tools\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Jq for bash 
if [[ $(which jq) ]]; then
    echo -e "${CLR_GRN}[INFO]    Jq installed\t\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Jq not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install -y jq ...${RESET_ALL}"
    sudo apt-get install -y jq
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install jq\t${FAILURE}${RESET_ALL}"
        exit 8
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed jq\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Python3
if [[ $(which python3) ]]; then
    echo -e "${CLR_GRN}[INFO]    Python3 installed\t\t${SUCCESS}${RESET_ALL}"
    alias python=python3
else
    echo -e "${CLR_RED}[ERROR]   Please install python3.${RESET_ALL}"
    exit 9
fi

# Pip3
if [[ $(which pip3) ]]; then
    echo -e "${CLR_GRN}[INFO]    Pip3 installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Pip3 not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install -y python3-pip ...${RESET_ALL}"
    sudo apt-get install -y python3-pip
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install pip3\t${FAILURE}${RESET_ALL}"
        exit 10
        echo -e "${CLR_GRN}[INFO]    Successfully installed pip3\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Python libs 
# Requests 
if [[ $(pip3 list) == *"requests"* ]]; then
    echo -e "${CLR_GRN}[INFO]    Python requests lib installed\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Python requests library not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: pip3 install requests ...${RESET_ALL}"
    pip3 install requests
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Requests library\t${FAILURE}${RESET_ALL}"
        exit 11
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Requests library\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Serial
if [[ $(pip3 list) == *"serial"* ]]; then
    echo -e "${CLR_GRN}[INFO]    Python serial lib installed\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR-YLW}[WARN]    Python Serial library not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: pip3 install serial ...${RESET_ALL}"
    pip3 install serial
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install Serial library\t${FAILURE}${RESET_ALL}"
        exit 12
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed Serial library\t${SUCCESS}${RESET_ALL}"
    fi
fi

# PySerial
if [[ $(pip3 list) == *"pyserial"* ]]; then
    echo -e "${CLR_GRN}[INFO]    Python PySerial lib installed\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Python PySerial library not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: pip3 install pyserial ...${RESET_ALL}"
    pip3 install pyserial
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install PySerial library\t${FAILURE}${RESET_ALL}"
        exit 13
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed PySerial library\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Tmux
if [[ $(which tmux) ]]; then 
    echo -e "${CLR_GRN}[INFO]    Tmux installed\t\t${SUCCESS}${RESET_ALL}"
else
    echo -e "${CLR_YLW}[WARN]    Tmux not installed. Installing now."    
    echo -e "${CLR_YLW}[WARN]    Running: sudo apt-get install -y tmux ...${RESET_ALL}"
    sudo apt-get install -y tmux
    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}[ERROR]   Failed to install tmux\t${FAILURE}${RESET_ALL}"
        exit 14
    else 
        echo -e "${CLR_GRN}[INFO]    Successfully installed tmux\t${SUCCESS}${RESET_ALL}"
    fi
fi

# Setup cronjob to pull voxl-provisioning-node nightly
cp update_provisioning_node.sh ~
cronjob=$(sudo cat /var/spool/cron/crontabs/$USER)
if ! echo "$cronjob" | grep -q "update_provisioning_node.sh"; then
    echo -e "${CLR_YLW}[WARN]   Cron job not set up, setting up now...${RESET_ALL}"
    echo "0 4 * * * bash ~/update_provisioning_node.sh" | sudo tee -a /var/spool/cron/crontabs/$USER
    if [ $? -eq 0 ]; then
        echo -e "${CLR_GRN}[INFO]    Cron job set up\t\t${SUCCESS}${RESET_ALL}"
    else
        echo -e "${CLR_RED}[ERROR]    Cron job failed to set up.\t${FAILURE}${RESET_ALL}"
        echo -e "${CLR_RED}[ERROR]    Check user permissions or use the command 'crontab -e' and manually add the line '0 4 * * * bash ~/update_provisioning_node.sh' to the end of the file.${RESET_ALL}"
        # exit 15
    fi
else
    echo -e "${CLR_GRN}[INFO]    Cron job set up\t\t${SUCCESS}${RESET_ALL}"
fi

# Set up helper alias commands 
alias voxl="adb wait-for-device shell"
alias voxl-reboot="adb wait-for-device && adb reboot && adb wait-for-device shell"
source ~/.bashrc

echo -e "${CLR_GRN}[INFO]    Dependency check success!\t${SUCCESS}${RESET_ALL}"
echo ""

# Set up Gitlab SSH key for cron job
echo -e "${CLR_LIT_GRN}\nWould you like to set up a Gitlab SSH key? (This is required for voxl-provisioning-node cron job to work)${RESET_ALL}"
    select opt in "Yes" "No"; do
        case $opt in
            "Yes" )
                setup_gitlab_ssh_key
                break;;

            "No" )
                echo -e "${CLR_GRN}[INFO] Skipping Gitlab SSH key setup...\t${RESET_ALL}"
                break;;
            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;
        esac
    done


# Clone voxl-provisioning-node
echo -e "${CLR_LIT_GRN}\nWould you like to clone voxl-provisioning-node?${RESET_ALL}"
    select opt in "Yes" "No"; do
        case $opt in
            "Yes" )
                if ! [ -d ~/repos/voxl-provisioning-node ]; then 
                    echo -e "${CLR_GRN}\n[INFO] Cloning Voxl-provisioning-node...${RESET_ALL}"
                    cd ~/repos
                    git clone git@gitlab.modalai.com:modalai/voxl2-private/voxl-provisioning-node.git
                    if [ $? -eq 0 ]; then 
                        echo -e "${CLR_GRN}[INFO] Finished cloning Voxl-provisioning-node!\t${SUCCESS}${RESET_ALL}"
                    else 
                        echo -e "${CLR_RED}[ERROR] Failed to clone Voxl-provisioning-node.\t${FAILURE}${RESET_ALL}"
                    fi
                    cd - &>/dev/null
                else
                    echo -e "${CLR_GRN}[INFO] Voxl-provisioning-node already cloned!${RESET_ALL}"
                    echo -e "${CLR_GRN}[INFO] Double check that it was cloned using SSH so cronjob can git pull without prompting for user and password.${RESET_ALL}"
                fi
                break;;

            "No" )
                echo -e "${CLR_GRN}[INFO] Skipping voxl-provisioning-node clone...\t${RESET_ALL}"
                break;;
            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;
        esac
    done

# Clone device simulators
echo -e "${CLR_LIT_GRN}\nWould you like to clone device-simulators? (Used to create fake RC data for CI jobs)${RESET_ALL}"
    select opt in "Yes" "No"; do
        case $opt in
            "Yes" )
                if ! [ -d ~/repos/device-simulators ]; then 
                    echo -e "${CLR_GRN}\n[INFO] Cloning device-simulators repo...${RESET_ALL}"
                    cd ~/repos
                    git clone https://gitlab.modalai.com/modalai/px4/device-simulators.git
                    if [ $? -eq 0 ]; then 
                        echo -e "${CLR_GRN}[INFO] Finished cloning device-simulators!\t${SUCCESS}${RESET_ALL}"
                    else 
                        echo -e "${CLR_RED}[ERROR] Failed to clone device-simulators.\t${FAILURE}${RESET_ALL}"
                    fi
                    cd - &>/dev/null
                else
                    echo -e "${CLR_GRN}[INFO] Device-simulators already cloned!${RESET_ALL}"
                fi
                break;;

            "No" )
                echo -e "${CLR_GRN}[INFO] Skipping device-simulators clone...\t${RESET_ALL}"
                break;;
            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;
        esac
    done

# Set up Gitlab runner for CI jobs
echo ""
echo -e "${CLR_LIT_GRN}Would you like to register a Gitlab runner?${RESET_ALL}"
    select opt in "Yes" "No"; do
        case $opt in
            "Yes" )
                register_gitlab_runner
                break;;

            "No" )
                echo -e "${CLR_GRN}[INFO] Skipping Gitlab runner setup...\t${RESET_ALL}"
                echo ""
                echo -e "${CLR_GRN}[INFO] CI setup successful!\t${SUCCESS}${RESET_ALL}"
                break;;
            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;
        esac
    done
